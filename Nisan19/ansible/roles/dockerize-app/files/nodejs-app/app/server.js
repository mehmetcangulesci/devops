let express = require('express');
let path = require('path');
let MongoClient = require('mongodb').MongoClient;
let bodyParser = require('body-parser');
let cors = require('cors');
let app = express();

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

let username = process.env.MONGO_DB_USERNAME;
let password = process.env.MONGO_DB_PASSWORD;
let mongoUrl = `mongodb://${username}:${password}@mongodb`;

let mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };
let databaseName = "my-db";

app.get('/get-profile', function (req, res) {
  let response = {};
  MongoClient.connect(mongoUrl, mongoClientOptions, function (err, client) {
    if (err) throw err;
    let db = client.db(databaseName);
    let myquery = { userid: 1 };
    db.collection("users").findOne(myquery, function (err, result) {
      if (err) throw err;
      response = result;
      client.close();
      res.set({
        'Access-Control-Allow-Origin': '*'
      }).send(response ? response : {});
    });
  });
});

app.listen(3000, function () {
  console.log("App listening on port 3000!");
});

