# DevOps Case Studies (Apr. 19)
[![user][user_img]][user_url]

[user_img]:https://img.shields.io/badge/made%20by-mehmetcangulesci-orange.svg
[user_url]:https://gitlab.com/mehmetcangulesci

[Vagrant](https://www.vagrantup.com/) is an open-source software product for building and maintaining portable virtual software development environments. So I've decided to use it for creating virtual servers.

According to `Vagrantfile`, I've created three virtual servers with the below specs.

**`Case 1`**

|      | Box | Hostname |  IP |
| :--- | :---: | :---: | :---: |
| **vm** | centos/7 | case1 | 192.168.135.111 |

<br>

**`Case 2`**

|      | Box | Hostname |  IP |
| :--- | :---: | :---: | :---: |
| **vm-1** | ubuntu/focal64 | ansible_control | 192.168.135.110 |
| **vm-2** | centos/7 | case2 | 192.168.135.112 |

<br>

[Ansible](https://www.ansible.com/) is the simplest way to automate apps and IT infrastructure. I've used [Ansible role](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) technique in order to easily reuse them and ensure well-structured directory design.

---

### Case 1
When servers run, facing vagrant user as default so add `sudo` privileges to run below commands.
#### Step 1.1
```bash
[vagrant@case1 ~]$ sudo yum update -y
```
#### Step 1.2
```bash
[vagrant@case1 ~]$ sudo useradd mehmetcan.gulesci
[vagrant@case1 ~]$ sudo passwd mehmetcan.gulesci
.... #password process
[vagrant@case1 ~]$ sudo visudo
```
Add below line to `/etc/sudoers` file or in file which open with `visudo` command in order to ensure related user run any command anywhere.
> mehmetcan.gulesci ALL=(ALL) ALL

```bash
[vagrant@case1 ~]$ sudo su - mehmetcan.gulesci
```
#### Step 1.3
**Purpose:** Add 10GB secondary disk mounted as `/bootcamp`
Type list block devices command.
```bash
[mehmetcan.gulesci@case1 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
```

To create secondary disk from VirtualBox GUI, below instructions should be considered.

<a href="shots/disk_cr_process_1.png"><img src="shots/disk_cr_process_1.png" width="30%" /></a> <b>&#8594;</b>
<a href="shots/disk_cr_process_2.png"><img src="shots/disk_cr_process_2.png" width="30%" /></a> <b>&#8594;</b>
<a href="shots/disk_cr_process_3.png"><img src="shots/disk_cr_process_3.png" width="30%" /></a> 
<a href="shots/disk_cr_process_4.png"><img src="shots/disk_cr_process_4.png" width="30%" /></a> <b>&#8594;</b>
<a href="shots/disk_cr_process_5.png"><img src="shots/disk_cr_process_5.png" width="30%" /></a> <b>&#8594;</b>
<a href="shots/disk_cr_process_6.png"><img src="shots/disk_cr_process_6.png" width="30%" /></a>  

After the above instructions `sdb` disk would be seen.
```bash
[mehmetcan.gulesci@case1 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0  10G  0 disk
```

To format the disk partition, get the device name from the previous `lsblk` command.
```bash
[mehmetcan.gulesci@case1 ~]$ sudo fdisk /dev/sdb

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for mehmetcan.gulesci:
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table
Building a new DOS disklabel with disk identifier 0x5ccb1b73.

Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
Partition number (1-4, default 1):
First sector (2048-20971519, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-20971519, default 20971519):
Using default value 20971519
Partition 1 of type Linux and of size 10 GiB is set

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Create a `/bootcamp` folder which disk will be mounted on.
```bash
[mehmetcan.gulesci@case1 ~]$ sudo mkdir /bootcamp
```

Make file system for newly created disk partition.
```bash
[mehmetcan.gulesci@case1 ~]$ sudo mkfs.xfs /dev/sdb1
meta-data=/dev/sdb1              isize=512    agcount=4, agsize=655296 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=2621184, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
```

Mount disk to `/bootcamp` folder
```bash
[mehmetcan.gulesci@case1 ~]$ sudo mount /dev/sdb1 /bootcamp
```

Now, mountpoint for sdb1 partition has been determined. 
```bash
[mehmetcan.gulesci@case1 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0  10G  0 disk
└─sdb1   8:17   0  10G  0 part /bootcamp
```
#### Step 1.4
```bash
[mehmetcan.gulesci@case1 ~]$ sudo chown -R mehmetcan.gulesci:mehmetcan.gulesci /opt
```
```bash
[mehmetcan.gulesci@case1 ~]$ mkdir /opt/bootcamp
[mehmetcan.gulesci@case1 ~]$ ll /opt/
total 0
drwxrwxr-x. 2 mehmetcan.gulesci mehmetcan.gulesci 6 Apr 23 14:12 bootcamp
```
```bash
[mehmetcan.gulesci@case1 ~]$ echo "merhaba trendyol" > /opt/bootcamp/bootcamp.txt
[mehmetcan.gulesci@case1 ~]$ ll /opt/bootcamp/
total 4
-rw-rw-r--. 1 mehmetcan.gulesci mehmetcan.gulesci 17 Apr 23 14:13 bootcamp.txt
```
#### Step 1.5
```bash
[mehmetcan.gulesci@case1 ~]$ sudo find /* -name bootcamp.txt -exec mv -t /bootcamp {} \;
[mehmetcan.gulesci@case1 ~]$ ll /bootcamp/
total 4
-rw-rw-r--. 1 mehmetcan.gulesci mehmetcan.gulesci 17 Apr 23 14:15 bootcamp.txt
[mehmetcan.gulesci@case1 ~]$ ll /opt/bootcamp/
total 0
```

---

### Case 2
Prerequisites to run and connect both servers related to `Case 2`.

- Run `vagrant up` command while in the same directory with `Vagrantfile`.

- Connect `ansible-control` machine (master) to run ansible commands.
```bash
➜  Nisan19 git:(master) ✗ vagrant ssh ansible-control
```

- Connect `case2` machine (worker) which ansible commands will be applied.
```bash
➜  Nisan19 git:(master) ✗ vagrant ssh case2
```

`MASTER MACHINE`

Change directory to ansible folder.
```bash
vagrant@ansible-control:~$ cd /vagrant/ansible
```

```bash
vagrant@ansible-control:/vagrant/ansible$ tree
.
├── ansible.cfg
├── hosts
├── roles
│   ├── docker
│   │   ├── tasks
│   │   │   └── main.yml
│   │   └── vars
│   │       └── main.yml
│   ├── dockerize-app
│   │   ├── files
│   │   │   └── nodejs-app
│   │   │       ├── Dockerfile
│   │   │       ├── app
│   │   │       │   ├── index.html
│   │   │       │   ├── package.json
│   │   │       │   └── server.js
│   │   │       └── docker-compose.yaml
│   │   └── tasks
│   │       └── main.yml
│   ├── nginx
│   │   ├── tasks
│   │   │   └── main.yml
│   │   └── templates
│   │       ├── app.conf
│   │       ├── index.html
│   │       └── nginx.conf
│   └── prerequisite
│       └── tasks
│           └── main.yml
└── site.yml

14 directories, 16 files
```

`hosts` file

```
[case2]
case2 ansible_host=192.168.135.112

[case2:vars]
ansible_python_interpreter=/usr/bin/python
```

#### Role descriptions
##### `prerequisite`
- Includes prerequisite commands to install other roles. (Disabling SELinux, Install Epel Release etc.)
##### `docker`
 - Includes docker and docker-compose installation commands.
##### `dockerize-app`
- Includes nodejs api compatible with `Express.js` (Framework) and `MongoDb` (Database).
- To dockerize the node-app, `Dockerfile` has been placed to the same directory.

```Dockerfile
FROM node:13-alpine
COPY app/* /usr/app/
WORKDIR /usr/app
RUN npm install
EXPOSE 3000
CMD ["node", "server.js"]
```

- To run the docker container, `docker-compose.yml` which includes built app and other two dependencies `MongoDb` and `Mongo Express` image, has been placed to the same directory with node-app.

```yml
version: '3'
services:
  node-app:
    image: node-app
    container_name: node-app
    ports:
     - 3000:3000
    environment:
     - MONGO_DB_USERNAME=${MONGO_DB_USERNAME}
     - MONGO_DB_PASSWORD=${MONGO_DB_PASSWORD}
  mongodb:
    image: mongo
    container_name: mongodb
    ports:
     - 27017:27017
    environment:
     - MONGO_INITDB_DATABASE=my-db
     - MONGO_INITDB_ROOT_USERNAME=${MONGO_DB_USERNAME}
     - MONGO_INITDB_ROOT_PASSWORD=${MONGO_DB_PASSWORD}
    volumes:
     - mongo-data:/data/db
  mongo-express:
    image: mongo-express
    container_name: mongo-express
    ports:
     - 8080:8081
    environment:
     - ME_CONFIG_MONGODB_ADMINUSERNAME=${MONGO_DB_USERNAME}
     - ME_CONFIG_MONGODB_ADMINPASSWORD=${MONGO_DB_PASSWORD}
     - ME_CONFIG_MONGODB_SERVER=mongodb
volumes:
  mongo-data:
    driver: local
```
##### `nginx`
- Includes nginx installation and some configuration commands.
- `index.html` file existing in templates folder which will be inserted in `/usr/share/nginx/html/index.html` in worker machine.
```html
<h1>Hosgeldin Devops</h1>
```
- Newly configured `nginx.conf` file (removed blocks are server:80 and server:443) existing in templates folder which will be inserted in `/etc/nginx/nginx.conf` in worker machine.
- `app.conf` file existing in templates folder which will be inserted in `/etc/nginx/conf.d/` in worker machine. It will ensure that to pass static page stated above if container application takes following header `'bootcamp: devops'`.
```bash
server {
  listen 80;
   
  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;

    if ($http_bootcamp != 'devops') {
      proxy_pass http://127.0.0.1:3000;
    }
  }
    
  error_page 500 502 503 504 /50x.html;
  location = /50x.html {
    root html;
  }
}
```

---
#### `ansible command`

```bash
vagrant@ansible-control:/vagrant/ansible$ ansible-playbook -i hosts site.yml
```


```bash
PLAY [Case-2 studies] ********************************************************************

TASK [Gathering Facts] *******************************************************************
ok: [case2]

TASK [prerequisite : Disable SELinux] ****************************************************
[WARNING]: SELinux state temporarily changed from 'enforcing' to 'permissive'. State
change will take effect next reboot.
changed: [case2]

TASK [prerequisite : Install EPEL repo] **************************************************
changed: [case2]

TASK [prerequisite : Install pip] ********************************************************
changed: [case2]

TASK [prerequisite : Install docker-py] **************************************************
changed: [case2]

TASK [docker : Install Docker pre-requisite packages] ************************************
changed: [case2] => (item=yum-utils)
changed: [case2] => (item=device-mapper-persistent-data)
changed: [case2] => (item=lvm2)

TASK [docker : Add Docker repo] **********************************************************
[WARNING]: Module remote_tmp /root/.ansible/tmp did not exist and was created with a mode
of 0700, this may cause issues when running as another user. To avoid this, create the
remote_tmp dir with the correct permissions manually
changed: [case2]

TASK [docker : Install Docker] ***********************************************************
changed: [case2]

TASK [docker : Start Docker service] *****************************************************
changed: [case2]

TASK [docker : Add user vagrant to docker group] *****************************************
changed: [case2]

TASK [docker : Ensure docker-compose is installed and available] *************************
changed: [case2]

TASK [dockerize-app : Create build directory] ********************************************
changed: [case2]

TASK [dockerize-app : Copy nodejs-app to server] *****************************************
changed: [case2]

TASK [dockerize-app : Build docker container image] **************************************
[WARNING]: The default for build.pull is currently 'yes', but will be changed to 'no' in
Ansible 2.12. Please set build.pull explicitly to the value you need.
changed: [case2]

TASK [dockerize-app : Run docker images] *************************************************
changed: [case2]

TASK [nginx : Install nginx] *************************************************************
changed: [case2]

TASK [nginx : Copy static index.html] ****************************************************
changed: [case2]

TASK [nginx : Overwrite default nginx config file] ***************************************
changed: [case2]

TASK [nginx : Create app.conf file] ******************************************************
changed: [case2]

TASK [nginx : Start Nginx] ***************************************************************
changed: [case2]

PLAY RECAP *******************************************************************************
case2                      : ok=20   changed=19   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

`WORKER MACHINE`

```bash
[vagrant@case2 ~]$ docker -v
Docker version 20.10.6, build 370c289
```
```bash
[vagrant@case2 ~]$ docker-compose -v
docker-compose version 1.29.0, build 07737305
```
```bash
[vagrant@case2 ~]$ nginx -v
nginx version: nginx/1.16.1
```
```bash
[vagrant@case2 ~]$ docker images
REPOSITORY      TAG         IMAGE ID       CREATED         SIZE
node-app        latest      96bf0c534355   5 minutes ago   122MB
mongo-express   latest      51fc3f2af7a1   8 days ago      128MB
mongo           latest      30b3be246e39   2 weeks ago     449MB
node            13-alpine   8216bf4583a5   11 months ago   114MB
```
```bash
[vagrant@case2 ~]$ docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED         STATUS         PORTS                                           NAMES
6c99cc04daed   node-app        "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp       node-app
0d3a0d92ac21   mongo           "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   0.0.0.0:27017->27017/tcp, :::27017->27017/tcp   mongodb
1274c91679f8   mongo-express   "tini -- /docker-ent…"   4 minutes ago   Up 4 minutes   0.0.0.0:8080->8081/tcp, :::8080->8081/tcp       mongo-express
```
Docker containerized Nodejs Application which includes default host `port:3000` has been served with nginx `port:80`.
![image_name](shots/nginx-app.png)


```bash
[vagrant@case2 ~]$ curl -i -X GET localhost:80
HTTP/1.1 200 OK
Server: nginx/1.16.1
Date: Fri, 23 Apr 2021 16:50:12 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 1167
Connection: keep-alive
X-Powered-By: Express
Access-Control-Allow-Origin: *
Accept-Ranges: bytes
Cache-Control: public, max-age=0
Last-Modified: Fri, 23 Apr 2021 16:32:07 GMT
ETag: W/"48f-178ff92ff58"

<html lang="en">
<style>
    .container {
        margin: 40px auto;
        width: 80%;
    }
    hr {
        width: 400px;
        margin-left: 0;
    }
    h3 {
        display: inline-block;
    }
    #container {
        display: none;
    }
</style>
<script>
    (async function init() {
        const response = await fetch(`/get-profile`);
        console.log("response", response);
        const user = await response.json();
        console.log(JSON.stringify(user));

        document.getElementById('name').textContent = user.name;
        document.getElementById('email').textContent = user.email;
        document.getElementById('interests').textContent = user.interests;

        const cont = document.getElementById('container');
        cont.style.display = 'block';
    })();

</script>
<body>
    <div class='container' id='container'>
        <h1>User profile</h1>
        <span>Name: </span><h3 id='name'>Mehmetcan Güleşçi</h3>
        <hr />
        <span>Email: </span><h3 id='email'>mehmetcan.gulesci@gmail.com</h3>
        <hr />
        <span>Interests: </span><h3 id='interests'>coding</h3>
        <hr />
    </div>
</body>
</html>
```

```bash
[vagrant@case2 ~]$ curl -i -X GET -H "bootcamp:devops" localhost:80
HTTP/1.1 200 OK
Server: nginx/1.16.1
Date: Fri, 23 Apr 2021 16:53:40 GMT
Content-Type: text/html
Content-Length: 25
Last-Modified: Fri, 23 Apr 2021 16:34:25 GMT
Connection: keep-alive
ETag: "6082f711-19"
Accept-Ranges: bytes

<h1>Hosgeldin Devops</h1>
```